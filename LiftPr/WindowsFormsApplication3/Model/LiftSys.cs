﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using IModel;

namespace Model
{
    public class LiftSys : ILiftSys
    {
        private Lift lift = new Lift();
        private List<Passenger> passengers = new List<Passenger>();
        private Thread mthread;
        public event EventHandler<EventArgs> information;
        public event EventHandler<EventArgs> status;
        public event EventHandler<EventArgs> statusLift;
        public event EventHandler<EventArgs> LiftStop;
        public event EventHandler<EventArgs> SetFinal;
        public event EventHandler<EventArgs> SetIn;

        public void addInformationLift(int numFloor, int capacity)
        {
            this.lift.setDateLift(capacity, numFloor);
        }//информация о системе

        public void InfPassenger(int id, int initialFloor, int finishFloor, int weight)
        {
            Passenger passenger = new Passenger();
            passenger.setInformation(id, initialFloor, finishFloor, weight);
            passengers.Add(passenger);
            if (this.information == null)
                return;
            this.information((object)this, EventArgs.Empty);
        } //информация о пассажирах + создание
        public string getInfomationPassenger()
        {
            string str = "";
            for (int index = 0; index < this.passengers.Count; ++index)
                str = str + "Passenger № " + this.passengers[index].idPass.ToString() + "; weight = " + this.passengers[index].weigtPass.ToString() + "; initFloor = " + this.passengers[index].inFloorPass.ToString() + "; FinalFloor = " + this.passengers[index].finFloorPass.ToString() + Environment.NewLine;
            return str;
        } //вывод инфы о пассажире
        public string getInformationPasEv()
        {
            string str1 = "";
            for (int index1 = 0; index1 < this.passengers.Count; ++index1)
            {
                int idPass;
                if (this.passengers[index1].createPass && !this.passengers[index1].positionPass && !this.passengers[index1].getDelete())
                {
                    string str2 = str1;
                    string[] strArray1 = new string[5];
                    strArray1[0] = str2;
                    strArray1[1] = "Passenger ";
                    string[] strArray2 = strArray1;
                    int index2 = 2;
                    idPass = this.passengers[index1].idPass;
                    string str3 = idPass.ToString();
                    strArray2[index2] = str3;
                    strArray1[3] = "call elevator ";
                    strArray1[4] = Environment.NewLine;
                    str1 = string.Concat(strArray1);
                }
                if (this.passengers[index1].positionPass && !this.passengers[index1].getDelete())
                {
                    string str2 = str1;
                    string[] strArray1 = new string[5];
                    strArray1[0] = str2;
                    strArray1[1] = "Passenger ";
                    string[] strArray2 = strArray1;
                    int index2 = 2;
                    idPass = this.passengers[index1].idPass;
                    string str3 = idPass.ToString();
                    strArray2[index2] = str3;
                    strArray1[3] = " in elevator ";
                    strArray1[4] = Environment.NewLine;
                    str1 = string.Concat(strArray1);
                }
                if (this.passengers[index1].getDelete())
                {
                    string str2 = str1;
                    string[] strArray1 = new string[5];
                    strArray1[0] = str2;
                    strArray1[1] = "Passenger " ;
                    string[] strArray2 = strArray1;
                    int index2 = 2;
                    idPass = this.passengers[index1].idPass;
                    string str3 = idPass.ToString();
                    strArray2[index2] = str3;
                    strArray1[3] = " exit elevator ";
                    strArray1[4] = Environment.NewLine;
                    str1 = string.Concat(strArray1);
                }
            }
            return str1;
        } //вывод инфы о действиях пасссажира
        public int getNowLift()
        {
            return this.lift.getNowFloorLift();
        }//вывод текущего этажа
        public int getNumberPassenger()
        {
            return this.passengers.Count;
        }
        public int GetNFloor()
        {
            return this.lift.getNumberFloor(); ;
        }//кол-во этажей
        public void Start()
        {
            mthread = new Thread(new ThreadStart(k));
            mthread.Start();
            if (SetIn != null)
                SetIn(this, EventArgs.Empty);
        }   //старт
        public void StopThread()
        {
            this.mthread.Abort();
            this.mthread.Join();
            if (SetFinal != null)
                SetFinal(this, EventArgs.Empty);
        } //стоп ( когда нажимаем срабатывает событие сетфинал, выводящее всю инфу в статистику
        private void k()
        {
            bool flag;
            do
            {
              flag = true;
            }
            while (this.passengers.Count <= 0);
            while (true)
           {
                //flag = true;
                this.thrLiftUP();
                this.thrLiftDown();
            }
        }
        int _numberMove=0; //кол-во передвижений
        int _PlusMove=0; //кол-во полезных передвижений
        public int SetNumberMove()
        {
            return _numberMove;
        } //вывод  _numberMove
        public int SetNumberFMove()
        {
            return _numberMove - _PlusMove;
        }//вывод холостых
        int SumW = 0;
        int NPass = 0;
        public int SumWeight()
        {
            return SumW;
        }
        public int NPassF()
        {
            return NPass;
        } //вывод довезенных пассажиров

        private void thrLiftUP()  
        {   
            int lifttomove = -1; // перемещение лифта к этажу
            int id = 0;  // номер пассажира                      
                // Пассажиры в лифте ?
                for (int i = 0; i < passengers.Count; i++)
                {
                    if (passengers[i].position == true)
                    {
                        lifttomove = passengers[i].finFloorPass;
                        id = i;
                        break;
                    }
                    else
                    {                       
                        lifttomove = -1;
                        continue;
                    }
                }                
                //Если нет в лифте забираем нового пассажира
                if (lifttomove == -1)
                {
                    for (int i = 0; i < passengers.Count; i++)
                    {
                        if (passengers[i].position == false)
                        {
                            id = i;
                            lifttomove = passengers[id].inFloorPass; 
                            break;
                        }
                    }
                    // ближайшего к лифту
                    for (int i = 0; i < passengers.Count; i++)
                    {
                        if (passengers[id].inFloorPass > passengers[i].inFloorPass && passengers[i].position == false)
                        {
                            lifttomove = passengers[i].inFloorPass;
                            id = i;
                        }
                        else continue;
                    }
                }                       
            // Если пассажир в ту сторону,то едем
            // Если нет, меняем направления движения
            if (lifttomove > -1 && lifttomove >= lift.initFloor())
            {
                //int oldcount = passengers.Count;
                while (true)
                {    
                    if (status != null) // обновляем статус пассажиров во 2-м окне
                        status(this, EventArgs.Empty);
                    int k = lifttomove;
                    for (int i = lift.initFloor(); i < k; i++)
                    {   // начинаем движение к пассажиру 1
                        lift.move();
                        if (statusLift != null) // обновляем статус лифта во 2-м окне
                            statusLift(this, EventArgs.Empty);                       
                        // проверяем другие вызовы по пути
                        for (int j = 0; j < passengers.Count; j++)
                        {
                            if (lifttomove >= passengers[j].inFloorPass && passengers[j].inFloorPass >= lift.initFloor()) // между этажами
                            {
                                if (lift.initFloor() == passengers[j].inFloorPass && j != id) 
                                {   
                                    //passengers[j].PositionPassenger = true;
                                    k = passengers[j].inFloorPass; // если нашли то останавливаемся по пути
                                    // lifttomove = passengers[j].inFloorPass; 
                                    id = j;
                                }
                                if (status != null) // обновляем статус пассажиров во 2-м окне
                                    status(this, EventArgs.Empty);
                            }
                        }
                        for (int j = 0; j < passengers.Count; j++)
                        {
                            // Если нужный этаж пассажира, 
                           if (passengers[j].finFloorPass == lift.getNowFloorLift() && passengers[j].position == true)
                            {
                                SumW = SumW + passengers[j].weigtPass;
                                NPass++;
                                passengers[j].position = false;
                                passengers[j].Delete(); // выходит                             
                                if (status != null)
                                    status(this, EventArgs.Empty);
                                Thread.Sleep(3000);
                                passengers.RemoveAt(j); // удаляем
                                if(id >= j) id --; // уменьшаем номер пассажира, т.к данные в листе сдвинулись
        
                            }
                            else continue;
                        }

                        if (status != null) // обновляем статус пассажиров во 2-м окне
                            status(this, EventArgs.Empty);
                    }
                    if (LiftStop != null) // обновляем статус лифта во 2-м окне
                        LiftStop(this, EventArgs.Empty);


                  /*  if (oldcount != passengers.Count) break;*/ if (passengers.Count <= 0) break; // если никого не осталось меняем направление, скорее всего там тоже обратно направление поменяется
                    else if (passengers[id].position == false && passengers.Count > 0) // если лифт подьехал к пассажиру
                    {
                        // проверяем на перегруз
                        if (/*Overdrive(id) == false*/1 == 1)
                        {
                            passengers[id].position = true; // забираем его
                            _PlusMove++; // лифт был пустой, возможно, считаем как холостая поездка
                            if (lifttomove <= passengers[id].finFloorPass)
                            {
                                lifttomove = passengers[id].finFloorPass;
                            }
                        }                       
                        // забираем и других пассажиров на этом же этаже
                        for (int j = 0; j < passengers.Count; j++)
                        {
                            if (passengers[j].inFloorPass == lift.getNowFloorLift() && j != id && /*Overdrive(j) == false*/1 == 1)
                            {
                                passengers[j].position = true;
                                if (lifttomove <= passengers[j].finFloorPass)
                                {
                                    id = j;
                                    lifttomove = passengers[id].finFloorPass;
                                }
                            }
                        }
                        _numberMove++; // засчитывем как поездку
                    } // если лифт привез пассажира
                    else if (passengers[id].position == true)
                    {
                        //
                        SumW = SumW + passengers[id].weigtPass;
                        NPass++;
                        passengers[id].Delete();
                        passengers[id].position = false;
                        if (status != null)
                            status(this, EventArgs.Empty);
                        Thread.Sleep(3000);
                        passengers.RemoveAt(id); // высаживаем и удаляем
                        for (int j = 0; j < passengers.Count; j++)
                        {
                            if (lift.getNowFloorLift() == passengers[j].finFloorPass)
                            {
                                passengers[j].Delete();
                                passengers[j].position = false;
                                if (status != null)
                                    status(this, EventArgs.Empty);
                                Thread.Sleep(3000);
                                passengers.RemoveAt(j);
                                j--;
                                break;

                            }
                        }
                        if (status != null)
                            status(this, EventArgs.Empty);
                        // Довозим других пассажиров в лифте
                        for (int j = 0; j < passengers.Count; j++)
                        {
                            if (passengers[j].position == true && passengers[j].getDelete() == false && passengers[j].finFloorPass >= lift.initFloor())
                            {
                                id = j;
                                lifttomove = passengers[j].finFloorPass;
                                if (status != null)
                                    status(this, EventArgs.Empty);

                            }
                            if (passengers[j].inFloorPass >= lift.initFloor() && passengers[j].position == false && passengers[j].getDelete() == false)
                            {

                                id = j;
                                lifttomove = passengers[j].inFloorPass;
                                if (status != null)
                                    status(this, EventArgs.Empty);
                            }
                        }
                        _numberMove++;
                    }
                    if (lifttomove == lift.initFloor()) break; // не нашли никого, меняем направление
                }
            }
        }// Перемещение лифта вверх
        private void thrLiftDown()
        {
            int lifttomove = -1;
            int id = 0;            
                // Проверяем есть ли пассажиры в лифте
                for (int i = 0; i < passengers.Count; i++)
                {
                    if (passengers[i].position == true )
                    {
                        lifttomove = passengers[i].finFloorPass;
                        id = i;
                        break;
                    }
                    else
                    {
                        // Нет, идем дальше
                        lifttomove = -1;
                        continue;
                    }
                }
                //Если не нашли пассажира в лифте ищем нового пассажира
                if (lifttomove == -1)
                {
                    for (int i = 0; i < passengers.Count; i++)
                    {
                        if (passengers[i].position == false)
                        {
                            id = i;
                            lifttomove = passengers[id].inFloorPass;
                            break;
                        }
                    }
                    // ближайшего
                    for (int i = 0; i < passengers.Count; i++)
                    {
                        if (passengers[id].inFloorPass < passengers[i].inFloorPass && passengers[i].position == false)
                        {
                            lifttomove = passengers[i].inFloorPass;
                            id = i;
                        }
                        else continue;
                    }
                }            
            int k = lifttomove;
            if (lifttomove > -1 && lifttomove < lift.initFloor())
            {
                while (true)
                {
                    if (status != null)
                        status(this, EventArgs.Empty);

                   k = lifttomove;
                   // int oldLiftToMove = lifttomove;

                    for (int i = lift.initFloor(); i > k; i--)
                    {   // начинаем движение к пассажиру 1
                        lift.moveDown();
                        if (statusLift != null)
                            statusLift(this, EventArgs.Empty);
                        // проверяем вызовы по пути
                        for (int j = 0; j < passengers.Count; j++)
                        {
                            if (lifttomove <= passengers[j].inFloorPass && passengers[j].inFloorPass <= lift.initFloor() && passengers[j].getDelete() == false)
                            {
                                if (lift.initFloor() == passengers[j].inFloorPass && j != id)
                                {
                                    // passengers[j].positionPass = true;
                                    k = passengers[j].inFloorPass;
                                    //lifttomove = passengers[j].inFloorPass;
                                    id = j;
                                }
                                if (status != null)
                                    status(this, EventArgs.Empty);
                            }
                            // Если нужный этаж пассажира, выгоняем его
                            if (passengers[j].finFloorPass == lift.getNowFloorLift() && passengers[j].position == true && j != id)
                            {
                                SumW = SumW + passengers[j].weigtPass;
                                NPass++;
                                passengers[j].position = false;
                                passengers[j].Delete();
                                if (status != null)
                                    status(this, EventArgs.Empty);
                                Thread.Sleep(1000);
                                passengers.RemoveAt(j);
                                if(id >= j) id--;
                                Thread.Sleep(1000);

                            }
                            else continue;                        
                        }
                    }
                    if (LiftStop != null)
                        LiftStop(this, EventArgs.Empty);

                    if (passengers.Count <= 0) break;
                    else if (passengers[id].position == false)
                    {
                        if (/*Overdrive(id) == false*/1==1 && passengers[id].inFloorPass == lift.initFloor())
                        {
                            passengers[id].position = true;
                            _PlusMove++;
                            if (lifttomove >= passengers[id].finFloorPass)
                            {
                                lifttomove = passengers[id].finFloorPass;
                            }
                        }
                        else if (passengers[id].position == true)
                        {
                            for (int i = 0; i < passengers.Count; i++)
                            {
                                if (passengers[id].inFloorPass > passengers[i].inFloorPass && passengers[id].position == false &&
                                    passengers[i].getDelete() == false && i != id)
                                {
                                    lifttomove = passengers[i].inFloorPass;
                                    id = i;
                                }
                                else continue;
                            }
                        }
                        for (int j = 0; j < passengers.Count; j++)
                        {
                            if (passengers[j].inFloorPass == lift.getNowFloorLift() && j != id && /*Overdrive(j) == false*/1==1)
                            {
                                passengers[j].position = true;
                                if (lifttomove >= passengers[j].finFloorPass) lifttomove = passengers[id].finFloorPass;
                            }
                        }

                        _numberMove++;                       
                    }
                    else 
                    {
                        if (lift.getNowFloorLift() == passengers[id].finFloorPass)
                        {
                            SumW = SumW + passengers[id].weigtPass;
                            NPass++;
                            passengers[id].Delete();
                            passengers[id].position = false;
                            if (status != null)
                                status(this, EventArgs.Empty);
                            Thread.Sleep(1000);
                            passengers.RemoveAt(id);
                            break;
                        }

                        int oldId = id;
                        
                        for (int j = 0; j < passengers.Count; j++)
                        {
                            if(passengers[j].position == true && passengers[j].getDelete() == false && passengers[j].finFloorPass <= lift.initFloor())
                            {
                                id = j;
                                lifttomove = passengers[j].finFloorPass;
                                if (status != null)
                                    status(this, EventArgs.Empty);
                               
                            }

                            if (passengers[j].inFloorPass <= lift.initFloor() && passengers[j].position == false && passengers[j].getDelete() == false)
                            {

                                id = j;
                                lifttomove = passengers[j].inFloorPass;
                                if (status != null)
                                    status(this, EventArgs.Empty);
                            }
                        }
                        if (oldId == id) break;
                    }
                    if (lifttomove == lift.initFloor()) break;
                    _numberMove++;
                }
              }               
           }// А тут вниз
        }       
    }

       


