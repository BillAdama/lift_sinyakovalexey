﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IModel;

namespace Model
{
    public class Passenger: IPassenger
    {
        private int initfloor;
        private int finishfloor;
        private int weight;
        private bool create = true;
        public bool position = false;
        private int id;
        //public event EventHandler ManyWeight;
        //public event EventHandler InitFols;
        //public event EventHandler FinalFols;
        private bool delete;

        public void setInformation(int idPass, int initialFloor, int finalFloor, int weightpass)
        {
            this.id = idPass;
            this.initfloor = initialFloor;
            this.finishfloor = finalFloor;
            this.weight = weightpass;
        }
        public bool positionPass
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }

        public int idPass
        {
            get
            {
                return this.id;
            }
        }

        public bool createPass
        {
            get
            {
                return this.create;
            }
        }
        public int inFloorPass
        {
            get
            {
                return this.initfloor;
            }
        }

        public int finFloorPass
        {
            get
            {
                return this.finishfloor;
            }
        }

        public int weigtPass
        {
            get
            {
                return this.weight;
            }
        }
        public void Delete()
        {
            this.delete = true;
        }

        public bool getDelete()
        {
            return this.delete;
        }
       

    }
}
