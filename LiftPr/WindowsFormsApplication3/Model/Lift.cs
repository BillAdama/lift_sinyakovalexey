﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IModel;
using System.Threading;


namespace Model
{   

    public class Lift: ILift
    {
        public int numberFloor ;
        public int _Capacity ;
        public int realFloor = 1;
        private int initialFloor = 1;
       // public event EventHandler ManyFloor;
       // public event EventHandler ManyCapacity;
       // public event EventHandler<EventArgs> SetFloor;

        public int getNumberFloor()
        {
            return this.numberFloor;
        }

        public int initFloor()
        {
            return this.initialFloor;
        }

        public int getNowFloorLift()
        {
            return this.realFloor;
        }

        public void move()
        {
            Thread.Sleep(3000);
            ++this.realFloor;
            this.initialFloor = this.realFloor;
        }

        public void moveDown()
        {
            Thread.Sleep(3000);
            --this.realFloor;
            this.initialFloor = this.realFloor;
        }
  
        public void setDateLift(int Lcapasity, int NFloor)
        {
            this._Capacity = Lcapasity;
            this.numberFloor = NFloor;
        }

        /*public int GetNumberOfFloor
        {
            get
            {
                return numberFloor;
            }
            set
            {
                if (0 < value && value <= 163)
                {
                    numberFloor = value; SetFloor(this, EventArgs.Empty);
                }
                else
                {
                    ManyFloor(this, EventArgs.Empty);

                }
            }
        }
        public void GetNumberOfFloorM(int a)
        {
            numberFloor = a;

            if (SetFloor != null)
                SetFloor(this, EventArgs.Empty);
        }
        public int SetNumberFloorM()
        {
            return numberFloor;
        }*/
      /*  public int GetCapacity
        {
            get
            {
                return _Capacity;
            }
            set
            {
                if (0 < value && value <= 800)
                    _Capacity = value;
                else
                { ManyCapacity(this, EventArgs.Empty);  }
            }
        }*/
    }
}
