﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IView
{
    public interface IFinalForm
    {
        void SetNumberMove(int NumberOfMove);
        void SetNumberFMove(int NumberOfFMove);
        void SetWeight(int Weight);
        void SetNumPass(int NumP);

    }
}
