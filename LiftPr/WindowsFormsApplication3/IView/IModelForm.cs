﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IView
{
    public interface IModelForm
    {
        void SetNumberOfFloor(int NumberOfFloor);
        void addInfPassenger(string information);
        void addStatusPassenger(string information);
        void addStopLift();
        //void addNumberFloor(int numfloor);
        void addMoveLift(int num);
    }
}
