﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IView
{
    public interface IMainFrame
    {
       event EventHandler StartClick;
       event EventHandler<EventArgs> CreateClick;
       event EventHandler StopClick;

        int NumberOfFloor { get; }
        int Capacity { get; }
        int FinalFloor { get; }
        int InitFloor { get; }
        int Weight { get; }
        void Error(int number);
    }
}
