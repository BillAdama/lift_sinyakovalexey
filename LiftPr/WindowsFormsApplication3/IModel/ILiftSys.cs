﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IModel
{
    public interface ILiftSys
    {
        event EventHandler<EventArgs> information;
        event EventHandler<EventArgs> status;
        event EventHandler<EventArgs> statusLift;
        event EventHandler<EventArgs> LiftStop;
        event EventHandler<EventArgs> SetFinal;
        event EventHandler<EventArgs> SetIn;
        void addInformationLift(int numFloor, int capacity);
        void InfPassenger(int id, int initialFloor, int finishFloor, int weight);
        string getInfomationPassenger();
        string getInformationPasEv();
       // int getLiftInformation();
        int getNowLift();
        void Start();
        void StopThread();
        int SetNumberMove();
        int SetNumberFMove();
        int SumWeight();
        int NPassF();
        int GetNFloor();
    }
}
