﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IModel
{
    public interface IPassenger
    {
       // event EventHandler ManyWeight;
       // event EventHandler InitFols;
       // event EventHandler FinalFols;

        int idPass { get; }
        int inFloorPass { get; }
        void Delete();
        bool getDelete();
        int finFloorPass { get; }
        int weigtPass { get; }
        bool createPass { get; }
        void setInformation(int idPass, int initialFloor, int finalFloor, int weightpass);
    }
}
