﻿namespace WindowsFormsApplication3
{
    partial class ModelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.NumFloor = new System.Windows.Forms.Label();
            this.InfPass = new System.Windows.Forms.TextBox();
            this.InfPass2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(269, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Текущий этаж";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(381, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "1";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(206, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(269, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Количество этажей";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // NumFloor
            // 
            this.NumFloor.AutoSize = true;
            this.NumFloor.Location = new System.Drawing.Point(381, 30);
            this.NumFloor.Name = "NumFloor";
            this.NumFloor.Size = new System.Drawing.Size(13, 13);
            this.NumFloor.TabIndex = 8;
            this.NumFloor.Text = "0";
            this.NumFloor.Click += new System.EventHandler(this.label7_Click);
            // 
            // InfPass
            // 
            this.InfPass.Location = new System.Drawing.Point(87, 118);
            this.InfPass.Multiline = true;
            this.InfPass.Name = "InfPass";
            this.InfPass.Size = new System.Drawing.Size(365, 187);
            this.InfPass.TabIndex = 9;
            this.InfPass.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // InfPass2
            // 
            this.InfPass2.Location = new System.Drawing.Point(499, 118);
            this.InfPass2.Multiline = true;
            this.InfPass2.Name = "InfPass2";
            this.InfPass2.Size = new System.Drawing.Size(119, 187);
            this.InfPass2.TabIndex = 10;
            this.InfPass2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // ModelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 326);
            this.Controls.Add(this.InfPass2);
            this.Controls.Add(this.InfPass);
            this.Controls.Add(this.NumFloor);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Name = "ModelForm";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.ModelForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label NumFloor;
        private System.Windows.Forms.TextBox InfPass;
        private System.Windows.Forms.TextBox InfPass2;
    }
}