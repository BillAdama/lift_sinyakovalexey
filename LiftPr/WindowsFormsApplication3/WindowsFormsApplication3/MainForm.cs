﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IView;

namespace WindowsFormsApplication3
{ 
    public partial class MainForm : Form, IMainFrame
    {
    
        public MainForm()
        {
            InitializeComponent();
        }
        public void Error(int number)
        {
            if (number == 1) MessageBox.Show("Неправильные данные о системе", "Важное сообщение !", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (number == 2) MessageBox.Show("Система уже создана", "Важное сообщение !", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (number == 3) MessageBox.Show("Неправильные данные о пассажире", "Важное сообщение !", MessageBoxButtons.OK, MessageBoxIcon.Information);
            if (number == 4) MessageBox.Show("Сначала создайте систему", "Важное сообщение !", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        public event EventHandler StartClick;
        private void Start_Click(object sender, EventArgs e) // start
        {
            if (StartClick != null) StartClick(this, EventArgs.Empty);

        }

       /* private void Stat_Click(object sender, EventArgs e) // stat
        {
           // FinalForm newForm = new FinalForm();
           // newForm.Show();
        }*/
        public event EventHandler StopClick;
        private void Stop_Click(object sender, EventArgs e) // stop
        {
           // ModelForm newForm = new ModelForm();
          //  newForm.Hide();
          // FinalForm newForm2 = new FinalForm();
          // newForm2.Show();
           if (StopClick != null) StopClick(this, EventArgs.Empty);
        }
        public event EventHandler<EventArgs> CreateClick;
        private void Add_Click(object sender, EventArgs e) // add
        {
            if (CreateClick != null) CreateClick(this, EventArgs.Empty);
        }
        public int NumberOfFloor
        {
            get
           {
               return Convert.ToInt32(textBox1.Text);
            }
        }
   
        public void NumOfFloor_TextChanged(object sender, EventArgs e) //kol-vo etagei
        {

        }
        public int Capacity
        {
            get
            {
                return Convert.ToInt32(textBox2.Text);
            }
        }

        private void MaxWeight_TextChanged(object sender, EventArgs e) // gruzopod
        {

        }
        public int Weight
        {
            get
            {
                return Convert.ToInt32(textBox3.Text);
            }
        }

        private void Weight_TextChanged(object sender, EventArgs e) //ves pas
        {

        }
        public int InitFloor
        {
            get
            {
                return Convert.ToInt32(textBox4.Text);
            }
        }

        private void InitFloor_TextChanged(object sender, EventArgs e) // init floor
        {

        }
        public int FinalFloor
        {
            get
            {
                return Convert.ToInt32(textBox5.Text);
            }
        }

        private void FinalFloor_TextChanged(object sender, EventArgs e) // final floor
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }       
        private void button3_Click(object sender, EventArgs e)
        {
        }
    }
}
