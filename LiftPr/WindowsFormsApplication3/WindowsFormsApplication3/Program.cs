﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Presenters;
using Model;

namespace WindowsFormsApplication3
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            MainForm form1 = new MainForm();
            ModelForm form2 = new ModelForm();
            FinalForm form3 = new FinalForm();
            //MessServ messageService = new MessServ();
            Lift lif = new Lift();
            Passenger pass = new Passenger();
            LiftSys sys = new LiftSys();
            ModelPresenter moPresenter = new ModelPresenter(form2, lif, pass,sys);
            MainPresenter mPresenter = new MainPresenter(form1, lif, pass,sys);
            FinalPresenter fPresenter = new FinalPresenter(form3, sys);
            form3.Show();
            form2.Show();
            Application.Run(form1);

        }
    }
}
