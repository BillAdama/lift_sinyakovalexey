﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IView;
using IModel;

namespace Presenters
{
    public class MainPresenter
    {
        private IMainFrame _view;
        private ILift _model;
        private IPassenger _pass;
        private ILiftSys _LiftSys;
        private bool start = false;
        private int id=1;
       // public event EventHandler ForModel;

        public MainPresenter(IMainFrame view, ILift model, IPassenger pass, ILiftSys LiftSys)
        {
            _view = view;       
            _model =  model;
            _pass = pass;
            _LiftSys = LiftSys;

            _view.StartClick += new EventHandler(_view_StartClick);
            _view.CreateClick += new EventHandler<EventArgs>(_view_CreateClick);
            _view.StopClick += new EventHandler(this.StopSecondThread);

        }
        void _view_StartClick(object sender, EventArgs e)
        {
            if (start == false)
            {
                start = true;
                try
                {
                    _LiftSys.addInformationLift(this._view.NumberOfFloor, this._view.Capacity);
                    _LiftSys.Start();                  
                }

                catch 
                {
                    start = false;
                    _view.Error(1);                 
                }
            }
            else
            _view.Error(2);
        }

        void _view_CreateClick(object sender, EventArgs e)
        {
            if (start == true)
            {
                try
                {

                    if (_view.NumberOfFloor >= _view.InitFloor && _view.NumberOfFloor >= _view.FinalFloor && _view.Weight <= _view.Capacity)
                    {                      
                        _LiftSys.InfPassenger(id, this._view.InitFloor, this._view.FinalFloor, this._view.Weight);
                        //this._LiftSys.Start();
                        id++;
                    }
                    else
                    _view.Error(3);


                }

                catch 
                {
                    _view.Error(3);
                }
            }
            else
            _view.Error(4);
        }


        private void StopSecondThread(object sender, EventArgs e)
        {
            _LiftSys.StopThread();
        }
        private void Click11(object sender, EventArgs e)
        {
            _view.Error(4);
        }

    }
}
