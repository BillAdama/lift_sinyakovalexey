﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IView;
using IModel;

namespace Presenters
{
    public class FinalPresenter
    {
        private IFinalForm _view;
        private ILiftSys _LiftSys;
         public FinalPresenter(IFinalForm view, ILiftSys LiftSys)
        {
            _view = view;
            _LiftSys = LiftSys;
            _LiftSys.SetFinal += new EventHandler<EventArgs>(Set_Move);
        }
         public void Set_Move(object sender, EventArgs e)
         {
             _view.SetNumberMove(_LiftSys.SetNumberMove());
             _view.SetNumberFMove(_LiftSys.SetNumberFMove());
             _view.SetWeight(_LiftSys.SumWeight());
             _view.SetNumPass(_LiftSys.NPassF());
         }

    }
}
