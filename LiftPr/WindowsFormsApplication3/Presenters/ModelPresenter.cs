﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IModel;
using IView;

namespace Presenters
{
    public class ModelPresenter
    {
        private IModelForm _view;
        private ILift _model;
        private IPassenger _pass;
        private ILiftSys _LiftSystem;
        public ModelPresenter(IModelForm view, ILift model, IPassenger pass, ILiftSys LiftSystem)
        {
            _view = view;
            _model = model;
            _pass = pass;
            _LiftSystem = LiftSystem;
          // _model.SetFloor += new EventHandler<EventArgs>(Set_ModelFloor);
            _LiftSystem.information += new EventHandler<EventArgs>(this.InformationPassenger);
            _LiftSystem.status += new EventHandler<EventArgs>(this.CallLift);
            _LiftSystem.statusLift += new EventHandler<EventArgs>(this.LiftMove);
            _LiftSystem.LiftStop += new EventHandler<EventArgs>(this.liftStop);
            _LiftSystem.SetIn += new EventHandler<EventArgs>(Set_In);
        }
        private void InformationPassenger(object sender, EventArgs e)
        {
            _view.addInfPassenger(_LiftSystem.getInfomationPassenger());
           // this._view.addNumberFloor(this._LiftSystem.getLiftInformation());
        }
       /* public void Set_ModelFloor(object sender, EventArgs e)
        {
            _view.SetNumberOfFloor(_LiftSystem.GetNFloor());
        }*/
         public void Set_In(object sender, EventArgs e)
     {
     _view.SetNumberOfFloor(_LiftSystem.GetNFloor());
     }
        private void CallLift(object sender, EventArgs e)
        {
            _view.addStatusPassenger(this._LiftSystem.getInformationPasEv());
        }
        private void LiftMove(object sender, EventArgs e)
        {
            _view.addMoveLift(_LiftSystem.getNowLift());
        }

        private void liftStop(object sender, EventArgs e)
        {
            _view.addStopLift();
        }

    }
}
